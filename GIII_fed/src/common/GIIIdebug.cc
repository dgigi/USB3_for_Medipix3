#include "PCIDevice.hh"
#include "PCIAddressTable.hh"
#include "PCIAddressTableASCIIReader.hh"
//#include "PCIi386BusAdapter.hh"
#include "PCILinuxBusAdapter.hh"
//#include "PCIDummyBusAdapter.hh"
#include "PersistentCommandSequencer.hh"
#include "CommandSequenceASCIIReader.hh"

#include <cstdlib> 
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
using std::cin;
using std::exception;


#define MyrADDRESSTABLE "GIIIAddressMap.dat"
#define SEQUENCE_SETTINGS "Sequences.dat"
#define G3Myr_VENDORID 0xECD6 /**< needed to search for the device */
#define G3Myr_DEVICEID 0xFe05 /**< needed to search for the device */

using namespace HAL;
using namespace std;

int main(int argc,char *argv[]) {
 	
  try {
    // if you want to play with real hardware you need a real busAdapter:
    // change the comments below:

    PCILinuxBusAdapter busAdapter;
 
    PCIAddressTableASCIIReader GIIIaddressTableReader( MyrADDRESSTABLE );
    PCIAddressTable GIII_addressTable("Test address table",GIIIaddressTableReader  );
    PCIDevice GIII_Card(GIII_addressTable, busAdapter, G3Myr_VENDORID,G3Myr_DEVICEID,0);
 


//    PersistentCommandSequencer sequencer( SEQUENCE_SETTINGS, GIII_addressTable );

    bool loop = true;
    string item, name, fileName,file;
    char yes,buffer[8],tb,choix;
    uint32_t option, ic,i, data,value,top_bot;
    vector<string> names;

	choix = atoi(argv[1]);
	
	
	if (choix == 0) {
		 
		 
		// reset the card
		//GIII_Card.write ("reset", 0x1,HAL_NO_VERIFY,0);   //reset
 
		// ste the mask for the number of event descriptors
		GIII_Card.write("mask",0x0,HAL_NO_VERIFY,0);
		GIII_Card.write("Init_board",0x10300,HAL_NO_VERIFY,0);
 
		
		return 0;
		
       } 

 
     if (choix == 1) {
		 
		 
		// reset the card
//		GIII_Card.write ("reset", 0x1,HAL_NO_VERIFY,0);   //reset
		
		
		top_bot = 0x00000000;
	
		// load the descriptors
     	file = "event1.txt";
     	 
     			{
			ifstream FileStream( file.c_str() );
	
			cout << " load event ......wait !" <<endl;
			while (!(FileStream.eof()) )
				{
		 		FileStream.getline(buffer,10);
	 			istringstream Line(buffer);
	 			Line >> hex >> data;
         			cout << hex <<data <<endl;
	 			GIII_Card.write ("mem_start",data,HAL_NO_VERIFY,i);
	 			i=i+4;
				}
			FileStream.close();
			}
 			cout << " it's finished :" << i << " bytes" <<endl;
			
		// ste the mask for the number of event descriptors
		GIII_Card.write("mask",0x7,HAL_NO_VERIFY,0);
		GIII_Card.write("Init_board",0x10300,HAL_NO_VERIFY,0);
		
		
		// START the generator
		GIII_Card.read("Init_board",&value);
		//cout << hex << value << endl;
		value = value | 0x8000;
		//cout << hex << value << endl;
		GIII_Card.write("Init_board",value,HAL_NO_VERIFY,0);
		
		
		return 0;
		
       } 

     if (choix == 2 ) {

		//reset the card
		GIII_Card.write ("reset", 0x1,HAL_NO_VERIFY,0);   //reset
		return 0;
      }
    }
  
  catch ( HardwareAccessException& e ) {
    cout << "*** Exception occurred : " << endl;
    cout << e.what() << endl;
  } 
  catch ( exception e ) {
    cout << "*** Unknown exception occurred" << endl;
  }
  cout << "This is the end, my friend..." << endl;
  return 0;
}

